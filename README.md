# Web app for weather forecast 

Thanks for letting me take part in this test. 

I have completed the test as per the test intructions.Please follow the instructions below to get started.

Few points before getting started

- I have not made it responsive, the app looks best on resolution of 1300x700
- I have used some tools just so that I could complete the test faster, like using redux, redux saga were not total necesscity but I used them since I am more familiar with those.
- I have used typescript, again to speed things up.
- I have used the sample link provided for looking up weather, so the app will always give weather for a static date
- I could not find an icon for rain in the instructions, so I used an existing icon.
- I have added test only for the React component, tests for reducers and sagas could also have been added.

## Installation instructions

In the project directory, Please run  

### `npm install`

This will download all the necessary packages and dependencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.\
