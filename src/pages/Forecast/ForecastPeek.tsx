import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import WeatherIcon, { IconName } from '../../components/WeatherIcon';
import { setActiveForecast } from '../../core/actions';
import { State } from '../../core/reducers';
import { ForecastItem } from '../../models/weather';
import getTimePartFromDate from '../../utils/getTimePartFromDate';
import kelvinToCelsius from '../../utils/kelvinToCelcius';
type Props = {
    forecastItem: ForecastItem
}
const Container = styled.div<{ active?: boolean }>`
    margin: 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    border-radius: 3px;
    padding: 16px 0;
    cursor: pointer;
    ${({active}) =>  active ? 'background-color: #51557A;': '' }
    :hover{
        background-color: #3B3F69;
    }
`;
const TimeText = styled.p`
    color: #A8AABD;
    line-height: 28px;
    font-size: 24px;
`;
const TemperatureText = styled.p`
    font-family: 'Roboto-Bold';
    line-height: 50px;
    font-size: 44px;
`;

const ForecastPeek: React.FunctionComponent<Props> = ({ forecastItem }) => {
    const activeForecastItem = useSelector((state: State) => state.activeForecast);
    const dispatch = useDispatch();
    const onClick = () => {
        dispatch(setActiveForecast(forecastItem));
    }
    return (
        <Container onClick={onClick} active={activeForecastItem?.dt_txt === forecastItem.dt_txt}>
            <TimeText>{getTimePartFromDate(forecastItem.dt_txt)}</TimeText>
            <WeatherIcon size="small" iconName={forecastItem.weather[0].main.toLowerCase() as IconName} />
            <TemperatureText>{kelvinToCelsius(forecastItem.main.temp)}°</TemperatureText>
        </Container>
    );
}
export default ForecastPeek;