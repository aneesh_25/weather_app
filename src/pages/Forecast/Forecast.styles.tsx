import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
`;
export const LoaderContainer = styled.div`
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
`;
export const DetailContainer = styled.div`
    display: flex;
    justify-content: space-between; 
    >* {
        min-width: 300px;
        margin: 20px;
    }

`
export const PeekContainer = styled.div`
    display: flex;
    width: 100%;
    overflow: scroll;
`;
export const TextContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;
export const WeatherText = styled.div`
    display: flex;
    justify-content: space-between;
    color: #A8AABD;
    font-size: 28px;

`;
export const DateText = styled.div`
    font-family: 'Roboto-Bold';
    font-size: 72px;
    line-height: 100px;
`;
export const TemperatureText = styled.p`
    font-family: 'Roboto-Bold';
    line-height: 200px;
    font-size: 180px;

`;
export const ErrorText = styled.div`
    color: #A8AABD;
    font-size: 28px;
    margin-top: 28px;
    text-align: center;
`;