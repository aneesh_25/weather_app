import { connect} from 'react-redux';
import { Dispatch } from 'redux';
import { getForecast } from '../../core/actions';
import { State } from '../../core/reducers';
import Login from './Forecast';

const mapStateToProps = (state: State) => {
    return state;
}
const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        getForecast: () => {
            dispatch(getForecast());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)