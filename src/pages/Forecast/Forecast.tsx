import moment from 'moment';
import React, { useEffect } from 'react';
import Loader from '../../components/Loader';
import WeatherIcon from '../../components/WeatherIcon';
import { Forecast as ForecastModel, ForecastItem } from '../../models/weather';
import kelvinToCelsius from '../../utils/kelvinToCelcius';
import * as styles from './Forecast.styles';
import ForecastPeek from './ForecastPeek';
type Props = {
    forecast: ForecastModel | null,
    activeForecast: ForecastItem | null,
    loading: boolean,
    error: boolean,
    getForecast(): void
}

const Forecast: React.FunctionComponent<Props> = ({ forecast, loading, error, activeForecast, getForecast }) => {

    useEffect(() => {
        getForecast();
    }, []);
    if (error) {
        return <styles.ErrorText role="alert">Error! Could not load the weather info</styles.ErrorText>
    }
    if (loading || !forecast || !activeForecast) {
        return <styles.LoaderContainer title="Loading"><Loader height={360} width={360}/></styles.LoaderContainer>
    }

    return (
        <styles.Container>
            <styles.DetailContainer data-testid="detail">
                <div>
                    <WeatherIcon size="large" iconName="clear" />
                </div>
                <styles.TextContainer>
                    <styles.WeatherText>
                        <p>{activeForecast?.weather[0].main}</p>
                        <p>{kelvinToCelsius(activeForecast.main.temp_max)}°/{kelvinToCelsius(activeForecast.main.temp_min)}°</p>
                    </styles.WeatherText>
                    <styles.TemperatureText>
                        {kelvinToCelsius(activeForecast.main.temp)}°
                    </styles.TemperatureText>
                </styles.TextContainer>
                <styles.TextContainer>
                    <styles.WeatherText>
                        <p>{forecast.city.name}</p>
                    </styles.WeatherText>
                    <styles.DateText>
                        <p>{moment(activeForecast.dt).format('dddd')}</p>
                        <p>{moment(activeForecast.dt).format('DD.MMMM')}</p>
                    </styles.DateText>
                </styles.TextContainer>
            </styles.DetailContainer>
            <styles.PeekContainer>
                {forecast!.list.map(item => <ForecastPeek key={item.dt} forecastItem={item} />)}
            </styles.PeekContainer>
        </styles.Container>

    )
}
export default Forecast;