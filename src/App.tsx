import React from 'react';
import './App.css';
import Forecast from './pages/Forecast';

function App() {
  return (
    <Forecast/>
  );
}

export default App;
