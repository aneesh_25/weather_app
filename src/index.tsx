import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import reducers from './core/reducers';
import {rootSaga} from './core/saga';
import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, compose, createStore } from 'redux';
import { Provider } from 'react-redux';

const sagaMiddleware = createSagaMiddleware({
 
}); 
const composeEnhancers = // eslint-disable-next-line no-underscore-dangle
 ((window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && // eslint-disable-next-line no-underscore-dangle
 (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
     trace: true,
     traceLimit: 25
   })) ||
 compose;
const middleware = [sagaMiddleware];
 const enhancers = composeEnhancers(
  applyMiddleware(...middleware)
 );

 const store = createStore(reducers, undefined, enhancers);
 sagaMiddleware.run(rootSaga);


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
