import axios, { AxiosResponse } from 'axios';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import { Forecast } from '../models/weather';
import { GET_FORECAST, setError, setForecast, setLoading } from './actions';
const URL = '/data/2.5/forecast?q=Mumbai,DE&appid=b6907d289e10d714a6e88b30761fae22';

export function* watchGetForecast() {
    yield takeLatest(GET_FORECAST, getForecast)
}

function* getForecast() {
    try {
        yield put(setLoading(true));
        yield put(setError(false));
        const forecast: AxiosResponse<Forecast> = yield call([axios, axios.get], URL);
        yield put(setForecast(forecast.data));
        yield put(setLoading(false));
    } catch (error) {
        yield put(setError(true));
        yield put(setLoading(false));
    }
}

export function* rootSaga() {
    yield all([
        fork(watchGetForecast)
    ])
}