import produce from "immer";
import { Forecast, ForecastItem } from "../models/weather";
import { ActionTypes, SET_ACTIVE_FORECAST, SET_ERROR, SET_FORECAST, SET_LOADING } from "./actions";
export type State = {
    forecast: Forecast | null;
    activeForecast: ForecastItem | null;
    loading: boolean;
    error: boolean;
};
const INITIAL_STATE: State = {
    forecast: null,
    activeForecast: null,
    loading: false,
    error: false,
};

const reducer = (state = INITIAL_STATE, action: ActionTypes) => {
    return produce(state, (draft) => {
        switch (action.type) {
            case SET_FORECAST:
                draft.forecast = action.forecast;
                draft.activeForecast = action.forecast.list[0];
                return draft;
            case SET_ACTIVE_FORECAST:
                draft.activeForecast = action.activeForecast
                return draft
            case SET_LOADING:
                draft.loading = action.loading;
                return draft;
            case SET_ERROR:
                draft.error = action.error;
                return draft;
            default:
                return state;
        }
    });
};

export default reducer;
