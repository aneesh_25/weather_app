import { Forecast, ForecastItem } from "../models/weather";

export const GET_FORECAST = 'GET_FORECAST' as const;
export const SET_FORECAST = 'SET_FORECAST' as const;
export const SET_ACTIVE_FORECAST = 'SET_ACTIVE_FORECAST' as const;
export const SET_LOADING = 'SET_LOADING' as const;
export const SET_ERROR = 'SET_ERROR' as const;

export const getForecast = () => {
    return {
        type: GET_FORECAST
    }
}

export const setForecast = (forecast: Forecast) => {
    return {
        type: SET_FORECAST,
        forecast
    }
}
export const setActiveForecast = (activeForecast: ForecastItem) => {
    return {
        type: SET_ACTIVE_FORECAST,
        activeForecast
    }
}
export const setLoading = (loading: boolean) => {
    return {
        type: SET_LOADING,
        loading
    }
}

export const setError = (error: boolean) => {
    return {
        type: SET_ERROR,
        error
    }
}

export type ActionTypes = ReturnType<typeof getForecast>
  | ReturnType<typeof setForecast>
  | ReturnType<typeof setActiveForecast>
  | ReturnType<typeof setLoading>
  | ReturnType<typeof setError>;