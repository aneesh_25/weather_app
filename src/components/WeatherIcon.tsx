import React from 'react';
import styled from 'styled-components';
import clear from '../assets/clear.svg'
import clouds from '../assets/clouds.svg'
import rain from '../assets/rain.svg'

export type IconName = 'clear'| 'clouds' | 'rain'
type Props = {
    iconName: IconName
    size: 'small'| 'large'
}
const SIZE_MAP = {
    small: '120',
    large: '256'
}
const ICON_MAP = {
    clear: clear,
    rain: rain,
    clouds: clouds
}
const Image = styled.img`
    color: #FFC700;
`;
const WeatherIcon: React.FunctionComponent<Props> = ({ iconName, size}) => {
    return <Image alt={iconName} src={ICON_MAP[iconName]} height={SIZE_MAP[size]} width={SIZE_MAP[size]}/>
}
export default WeatherIcon;