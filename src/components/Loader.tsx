import styled, { keyframes } from 'styled-components'
import { ReactComponent as Icon } from '../assets/clear.svg'
const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;
// const Icon = () => <img src={clear} alt="loader" />
const Loader = styled(Icon)`
  animation: ${rotate} 2s linear infinite;

`
export default Loader;