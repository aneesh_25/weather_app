function kelvinToCelsius(temperature: number){
    return Math.round(+temperature - 273.1)
}
export default kelvinToCelsius;