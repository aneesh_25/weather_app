import React from 'react';
import App from './App';
import { renderWithStore } from './test.utils';
import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { fireEvent, screen } from '@testing-library/react';

import testData from './testData.json';
const server = setupServer(

)
beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('should show loader in the beginning', async () => {
  server.use(rest.get(/\/data/gm, (req, res, ctx) => {
    return res(
      ctx.delay(5000),
      ctx.status(200),
      ctx.json(testData),
    )
  }))
  const { queryByTitle } = renderWithStore(<App />);
  expect(queryByTitle('Loading')).toBeInTheDocument();
});
test('In case of failure to get data should show error', async () => {
  server.use(rest.get(/\/data/gm, (req, res, ctx) => {
    return res(
      ctx.status(500),
      ctx.json({ message: 'Internal Server Error' }),
    )
  }))
  renderWithStore(<App />);
  const alert = await screen.findByRole('alert');

  expect(alert).toHaveTextContent('Error! Could not load the weather info')
  expect(screen.queryByTitle('Loading')).not.toBeInTheDocument();
});
test('should show default weather view', async () => {
  server.use(rest.get(/\/data/gm, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(testData),
    )
  }))
  renderWithStore(<App />);
  await screen.findAllByAltText('clear');
  expect(screen.getByTestId('detail')).toHaveTextContent('Clear14°/8°14°AltstadtSunday18.January');

});
test('on Selection of item should set it as selected item', async () => {
  server.use(rest.get(/\/data/gm, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(testData),
    )
  }))
  renderWithStore(<App />);
  await screen.findAllByAltText('clear');
  const button = screen.getByText('5:00').closest('div');
  fireEvent.click(button!);
  expect(screen.getByTestId('detail')).toHaveTextContent('Clear13°/9°13°AltstadtSunday18.January');
})

