import { applyMiddleware, createStore} from 'redux';
import { Provider} from 'react-redux';
import { render} from '@testing-library/react';
import createSagaMiddleware from 'redux-saga'

import reducer from './core/reducers';
import { rootSaga } from './core/saga';

const sagaMiddleware = createSagaMiddleware()

const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga)

export function renderWithStore(ui, options) {

    return render(
      <Provider store={store}>
       {ui}
      </Provider>,
      options
    );
   }